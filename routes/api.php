<?php

// use Illuminate\Http\Request;

use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::resource('users', UserController::class);
Route::resource('services', ServiceController::class);
Route::resource('payments', PaymentController::class);

Route::group(['prefix' => 'lawyer'], function () {
    Route::post('subscribe', [SubscriptionController::class, 'subscribe']);
    Route::post('showUserSubscriptions', [SubscriptionController::class, 'showUserSubscriptions']);
    Route::post('updateSubscriptionPayment', [SubscriptionController::class, 'updateSubscriptionPayment']);
    Route::post('unsubscribe', [SubscriptionController::class, 'unsubscribe']);
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('showActiveSubscriptions', [SubscriptionController::class, 'showActiveSubscriptions']);
    Route::get('showInactiveSubscriptions', [SubscriptionController::class, 'showInactiveSubscriptions']);
    Route::post('showSubscriptionDetails', [SubscriptionController::class, 'showSubscriptionDetails']);
    Route::post('cancelSubscription', [SubscriptionController::class, 'cancelSubscription']);
});
