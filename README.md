
# Subcription Test API

Es una API REST en Laravel que permita crear suscripciones de pago a los usuarios abogados y
procesar esos pagos 30 minutos después de creada la suscripción de forma recurrente, el API esta divida en
dos grupos: Abogados y Panel. Tambien permite procesar los pagos en segundo plano y enviar notificaciones vía Mail
y WebSocket.


## Instalacion

- Instalar el proyecto con composer install.
- Ejecutar el comando php artisan migrate.
- Buscar en el directorio del proyecto el archivo postman.
- Realizar la importacion de este archivo.
- Comenzar a usar la API.

## Autor

- [@kwgeniusz](https://gitlab.com/manuelcastro23)

