<?php

namespace App\Listeners;

use App\Events\PaymentRejected;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class BrodcastPaymentNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // broadcast(new PaymentRejected("Estimated: {$event->paymentNotifications}", $event->typeNotifications));
    }
}
