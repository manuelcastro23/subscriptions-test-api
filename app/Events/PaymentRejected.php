<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PaymentRejected implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $paymentNotifications;
    public $typeNotifications;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($paymentNotifications, $typeNotifications)
    {
        $this->paymentNotifications = $paymentNotifications;
        $this->typeNotifications = $typeNotifications;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        info("{$this->paymentNotifications}");
        info("{$this->typeNotifications}");
        return new Channel('payment-notifications');
    }
}
