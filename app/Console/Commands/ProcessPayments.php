<?php

namespace App\Console\Commands;

use App\Events\PaymentRejected;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProcessPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command processes subscription payments in the background.';

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function makePayment()
    {
        $options = collect([true, false]);

        return $options->random();
    }

    public function handle()
    {
        $payments = Payment::where('attemps', '<=', 2)->get();

        foreach ($payments as $payment) {

            $subscription = DB::table('service_user')->where('id', $payment->subscription_id)->first();

            if ($this->makePayment() == true) {
                DB::table('service_user')->where('id', $subscription->id)->update(['active' => 1]);
                Storage::append("pago_exitoso.txt", $subscription->id); //Cambiar esto por una notificacion. Meter aqui datos del usuario a notificar
            } else {
                $payment->update(['attemps' => $payment->attemps + 1]);
                $user = User::find($payment->user_id);
                event(new PaymentRejected($user->name, 'Failed Payment'));
                // Storage::append("pago_fallido.txt", $payment); //Cambiar esto por una notificacion. Meter aqui datos del usuario a notificar
            }
        }

        $unsubscribers = Payment::where('attemps', '>=', 3)->get();

        foreach ($unsubscribers as $unsubscribe) {

            $subscription = DB::table('service_user')->where('id', $unsubscribe->subscription_id)->first();

            DB::table('service_user')->delete($subscription->id);
            Storage::append("desuscripciones.txt", $subscription->id); //Cambiar esto por una notificacion. Meter aqui datos del usuario a notificar
        }
    }
}
