<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function index()
    {
        $services = User::paginate(5);

        return response()->json(['data' => $services], 200);
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->email_verified_at = now();
        $user->password = bcrypt($request->password);
        $user->remember_token = Str::random(10);

        $user->save();

        return response()->json(['data' => ['message' => 'User succesfully created.']], 201);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update([
            'name' => $request->name ? $request->name : $user->name,
            'email' => $request->email ? $request->email : $user->email,
            'password' => $request->password ? bcrypt($request->password) : $user->password,
        ]);

        return response()->json(['data' => ['message' => 'User succesfully updated.']], 201);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(['data' => ['message' => 'User succesfully deleted.']], 201);
    }
}
