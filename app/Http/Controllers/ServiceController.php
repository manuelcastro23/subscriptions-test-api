<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Http\Requests\StoreServiceRequest;
use App\Http\Requests\UpdateServiceRequest;

class ServiceController extends Controller
{

    public function index()
    {
        $services = Service::paginate(5);

        return response()->json(['data' => $services], 200);
    }

    public function store(StoreServiceRequest $request)
    {
        $service = new Service();

        $service->name = $request->name;
        $service->price = $request->price;
        $service->days = $request->days;

        $service->save();

        return response()->json(['data' => ['message' => 'Service succesfully created.']], 201);
    }

    public function update(UpdateServiceRequest $request, Service $service)
    {
        $service->update([
            'name' => $request->name ? $request->name : $service->name,
            'price' => $request->price ? $request->price : $service->price,
            'days' => $request->days ? $request->days : $service->days,
        ]);

        return response()->json(['data' => ['message' => 'Service succesfully updated.']], 201);
    }

    public function destroy(Service $service)
    {
        $service->delete();

        return response()->json(['data' => ['message' => 'Service succesfully deleted.']], 201);
    }
}
