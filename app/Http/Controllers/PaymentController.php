<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Http\Requests\StorePaymentRequest;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{

    public function index()
    {
        $payments = Payment::paginate(5);

        return response()->json(['data' => $payments], 200);
    }

    public function store(StorePaymentRequest $request)
    {
        $checkPayment = Payment::where('user_id', $request->user_id)->where('subscription_id', $request->subscription_id)->first();

        if ($checkPayment) {
            return response()->json(['data' => ['message' => 'Payment declined. The subscription has already been paid.']], 201);
        }

        $payment = new Payment();

        $payment->user_id = $request->user_id;
        $payment->subscription_id = $request->subscription_id;

        $payment->save();

        DB::table('service_user')->where('id', $request->subscription_id)->update(['active' => 1]);

        return response()->json(['data' => ['message' => 'The subscription has been successfully paid.']], 201);
    }
}
