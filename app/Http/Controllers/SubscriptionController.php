<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSubscriptionRequest;
use App\Http\Requests\UpdateSubscriptionRequest;
use App\Models\Payment;
use App\Models\Service;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SubscriptionController extends Controller
{
    public function showUserSubscriptions(Request $request)
    {
        Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
        ])->validated();

        $user = User::find($request->user_id);

        $subscriptions = User::find($request->user_id)->services()->get();

        return response()->json(['data' => ['user' => $user, 'subscriptions' => $subscriptions]], 201);
    }

    public function subscribe(StoreSubscriptionRequest $request) //
    {
        $user = User::find($request->user_id);
        $service = Service::find($request->service_id);

        if ($this->makePayment() == true) {

            $user->services()->attach([
                $request->service_id => [
                    'active' => 1,
                    'subscription_date' => Carbon::now(),
                    'expiration_date' => Carbon::now()->addDays($service->days),
                ]
            ]);

            //No hay una forma de obtener un id del registro creado con attach.
            //Truco para obtenerlo sacado de aqui: https://stackoverflow.com/a/63575470
            $lastId = DB::getPdo()->lastInsertId();

            $payment = new Payment();
            $payment->user_id = $request->user_id;
            $payment->subscription_id = $lastId;
            $payment->save();

            //Codigo para enviar notificacion aca...

            return response()->json(['data' => ['message' => 'The subscription has been created successfully.']], 201);
        } else {
            $user->services()->attach([
                $request->service_id => [
                    'active' => 0,
                    'subscription_date' => Carbon::now(),
                    'expiration_date' => Carbon::now()->addDays($service->days),
                ]
            ]);

            //Codigo para enviar notificacion aca...
            return response()->json(['data' => ['message' => 'Your subscription has been created but is not active. Please make the payment to activate it.']], 200);
        }
    }

    public function unsubscribe(UpdateSubscriptionRequest $request)
    {
        $user = User::find($request->user_id);

        $user->services()->detach($request->service_id);

        return response()->json(['data' => ['message' => 'The subscription been revoked successfully.']], 201);
    }

    public function makePayment()
    {
        $options = collect([true, false]);

        return $options->random();
    }

    public function updateSubscriptionPayment(Request $request)
    {
        Validator::make($request->all(), [
            'subscription_id' => 'required|integer|exists:service_user,id',
        ])->validated();

        $subscription = DB::table('service_user')->where('id', $request->subscription_id)->first();

        if ($subscription->active == 1) {
            return response()->json(['data' => ['message' => 'Payment declined. The subscription has already been paid.']], 200);
        }

        if ($this->makePayment() == true) {

            DB::table('service_user')->where('id', $request->subscription_id)->update(['active' => 1]);

            return response()->json(['data' => ['message' => 'Successfull payment.']], 200);
        } else {

            //Codigo para enviar notificacion aca...
            return response()->json(['data' => ['message' => 'Payment failed. Please try again.']], 200);
        }
    }

    public function showActiveSubscriptions()
    {
        $subscriptions = DB::table('service_user')->where('active', 1)->get();

        $data = $subscriptions->each(function ($subscription) {
            $user = User::find($subscription->user_id);
            $subscription->subscriber = $user;
        });

        return response()->json(['data' => ['active_subscriptions' => $data]], 200);
    }

    public function showInactiveSubscriptions()
    {
        $subscriptions = DB::table('service_user')->where('active', 0)->get();

        $data = $subscriptions->each(function ($subscription) {
            $user = User::find($subscription->user_id);
            $subscription->subscriber = $user;
        });

        return response()->json(['data' => ['inactive_subscriptions' => $data]], 200);
    }

    public function showSubscriptionDetails(Request $request)
    {
        $subscription = DB::table('service_user')->where('id', $request->subscription_id)->get();

        return response()->json(['data' => $subscription], 200);
    }

    public function cancelSubscription(Request $request)
    {
        Validator::make($request->all(), [
            'subscription_id' => 'required|integer|exists:service_user,id',
        ])->validated();

        DB::table('service_user')->where('id', $request->subscription_id)->update(['active' => 0]);

        return response()->json(['data' => ['message' => 'Subscription with number \'' . $request->subscription_id . '\' was successfully cancelled.']], 200);
    }
}
